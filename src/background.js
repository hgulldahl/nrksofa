// use strict;
console.log('background.js');

var settings  = {
    get: function(key) { return localStorage.getItem(key); },
    set: function(key, data) { return localStorage.setItem(key, data) },

};

var fb = {
    auth : function(appId) {
        console.log("Oauth FB");
        chrome.identity.launchWebAuthFlow(
        {'url': 'https://graph.facebook.com/oauth/access_token?client_id='+appId+
            '&redirect_uri=https://'+chrome.runtime.id+
            '.chromiumapp.org/fb_cb',
        'interactive': true},
            function(redirect_url) { 
            /* Extract token from redirect_url */ 
            console.log('identity redirect: %o', redirect_url);
            var token = redirect_url;
            settings.set('access_token', token);
            sendToScript({'access_token':token});
        });
   }
};

function sendToScript(msg, cb) {
    var _cb = cb || function(response) { console.log('got response: %o', response); };
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
        chrome.tabs.sendMessage(tabs[0].id, msg, _cb);
    });
}

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    console.log(sender.tab ?
                "from a content script:" + sender.tab.url :
                "from the extension");
    if (request.greeting == "hello")
      sendResponse({farewell: "goodbye"});
    if (request.fb_cmd == "auth") {
        fb.auth(request.fb_app_id); // start oauth
    }
    if (request.cmd == "playvideo") {
        console.log("playvideo: %o", request.url);
        chrome.windows.create({url:chrome.extension.getURL('head.html'), focused:true, type:'popup'},
                           function(win) { chrome.runtime.sendMessage({loadUrl:request.url}); }
                            );
    }
  });





