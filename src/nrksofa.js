// use strict;
console.debug('sofa.js');

var fbstatus, fbme, show;

var fbxhr = {
    access_token : '',
    me : function(cb) {
        jQuery.getJSON('https://graph.facebook.com/me?access_token='+fbxhr.access_token, cb);

    },
    like : function(uri, cb) {
        var q = jQuery.param({access_token: fbxhr.access_token,
                              method: 'POST',
                              object: uri});
        jQuery.getJSON('https://graph.facebook.com/me/og.likes?'+q,
                        {}, 
                       cb);
    },
    post_wants_to_watch : function(uri, id, cb) {
        var q = jQuery.param({access_token: fbxhr.access_token,
                              method: 'POST',
                              tv_show: uri});
        jQuery.getJSON('https://graph.facebook.com/me/nrksofa-lurtgjort-no:queue?'+q,
        //jQuery.getJSON('https://graph.facebook.com/video.wants_to_watch?'+q,
                        {}, 
                       cb);
    },
    get_wants_to_watch : function(cb) {
        var q = jQuery.param({access_token: fbxhr.access_token,
                              method: 'GET'});
        //jQuery.getJSON('https://graph.facebook.com/video.wants_to_watch?'+q,
        jQuery.getJSON('https://graph.facebook.com/nrksofa-lurtgjort-no:queue?'+q,
                       cb);
    }
};

function FlashPlayer() {
    /*
        var cmds = { // from player.flash.min.js
            seek: "JSsetSeekPosition",
            playStream: "JSplayStream",
            toggleplay: "JStogglePlayPause",
            play: "JSplay",
            pause: "JSpause",
            togglemute: "JStoggleMute",
            getDuration: "JSgetDuration",
            getPosition: "JSgetCurrentPosition",
            getPositionUTC: "JSgetCurrentPositionUTC",
            getPositionAbs: "JSgetCurrentPositionAbs",
            getBeginLiveTime: "JSgetBeginLiveTimeUTC",
            getEndLiveTime: "JSgetEndLiveTimeUTC",
            seekabs: "JSseekAbs",
            seekutc: "JSseekUTC",
            resize: "JSsetSize",
            lock: "JSlockPlayer",
            unlock: "JSunlockPlayer",
            getVersion: "JSgetVersion",
            updateQoS: "JSsetQoSdatas",
            setAnalyticsData: "JSsetAnalyticsCustomData",
            getPlayerInfo: "JSgetPlayerInfoJSON",
            getVideoInfo: "JSgetVideoInfoJSON"
        };
    */
    
    this.obj = document.getElementById('flashPlayer');
    console.debug('player instantiated: %o', this.obj);
}

FlashPlayer.prototype.play = function() {
    return this.obj.JSplay();
};
FlashPlayer.prototype.pause = function() {
    return this.obj.JSpause();
};
FlashPlayer.prototype.playpause = function() {
    return this.obj.JStogglePlayPause();
};
FlashPlayer.prototype.position = function() {
    return this.obj.JSgetCurrentPosition();
};
FlashPlayer.prototype.duration = function() {
    return this.obj.JSgetDuration();
};
FlashPlayer.prototype.lock = function() {
    return this.obj.JSlockPlayer();
};
FlashPlayer.prototype.unlock = function() {
    return this.obj.JSunlockPlayer();
};
FlashPlayer.prototype.seek = function(cuepoint) { // move playhead to cuepoint seconds (float) from the beginning
    return this.obj.JSsetSeekPosition(cuepoint);
};
FlashPlayer.prototype.info = function() { // get a lot of stats
    return JSON.parse(this.obj.JSgetPlayerInfoJSON());
};


var store = {
    set_user_access_token : function(t) {
        localStorage.setItem('sofa_fb_user_access_token', t);
    },
    get_user_access_token : function() {
        return localStorage.getItem('sofa_fb_user_access_token') || "";
    },
    set_user_id : function(id) {
        localStorage.setItem('sofa_fb_user_id', id);
    },
    get_user_id : function() {
        return localStorage.getItem('sofa_fb_user_id') || "";
    },
    save_playheadPosition : function(programid, playheadPosition) {
        localStorage.setItem(programid, JSON.stringify({pos:playheadPosition, timestamp:(new Date())}));
    },
    watch_later : function(programid) {
        var watchlist = store.get_watch_later();
        if(watchlist.indexOf(programid) == -1) { // ignore duplicates
            watchlist.push(programid);
            localStorage.setItem('watch_later', JSON.stringify(watchlist));
            $('body').trigger('watchlistupdated', [watchlist]);
        }
    },
    get_watch_later : function() {
        return JSON.parse(localStorage.getItem('watch_later') || "[]");
    }
};

//var headhtml = chrome.runtime.getURL('head.html');
jQuery('#op-msg-container').after('<div id=nrksofa></div><div id=fb-root></div>');
jQuery('#nrkno-link-wrapper').hide();
jQuery('#s').hide();

var openFavorites = function() {
    console.debug('open favorites');
};

var openGenerated = function() {
    console.debug('open Generated');
};

var openGenres = function() {
    console.debug('open Genres');
    jQuery('#nrksofa').animate({height:'400px'});
};

var openWatchList = function() {
    console.debug('openWatchList');
};

var toggleCarousel = function() {
    console.debug('open Carousel');
    $('#introSlider').toggle();
};

var playpause = function() {
    console.debug('playpause playpause');
    jQuery('#playerelement').trigger('playpause');
};

function goto(url) {
    document.location.href = url;
}

var login = function() {
 console.log(fbstatus);
 if(fbstatus.status == "connected") {
    return true;
 }
 FB.login(function(response) {
    console.log("loging responsE: %o", response);
 }, {scope: 'publish_actions,user_actions.video,user_actions:nrksofa-lurtgjort-no'});
}

function pullout(id) {
    jQuery(id) ;

}

function pullout_queue() {
    return pullout('#queue');
}

function pushin_queue() {
    return pushin('#queue');
}

// add top navigation
jQuery('<a id=sofa-favs class="lp-link nav-link" href=#>Favoritter</a>').click(openFavorites).prependTo('nav.global-nav');
jQuery('<a id=sofa-generated class="lp-link nav-link" href=#>For deg</a>').click(openGenerated).prependTo('nav.global-nav');
jQuery('<a id=sofa-genres class="lp-link nav-link" href=#>Sjangre</a>').click(openGenres).prependTo('nav.global-nav');
jQuery('<a id=sofa-settings class="lp-link nav-link" href=#>Dagens</a>').click(toggleCarousel).prependTo('nav.global-nav');
jQuery('<a id=sofa-watchlist class="lp-link nav-link" href=#>▤</a>').click(openWatchList).prependTo('nav.global-nav');
jQuery('<a id=sofa-fb-login class="lp-link nav-link" href=#>ƒ</a>').click(login).after('#nrksofa');
jQuery('<div class=pullout id=queue><h2>✪ kø</h2><ul></ul></div>')
    //.toggle(pullout_queue, pushin_queue)
    .insertAfter('#nrksofa');

jQuery('.lp-link.nav-link.programguide').hide();
jQuery('.lp-link.nav-link.live').hide();
//jQuery('#introSlider').hide();
jQuery('.stack-links').hide();

if(document.location.pathname.substring(0,6) == '/guide') {
    console.log('guide');
    jQuery('#programGuide h1').after('<ul class=buttonbar><li>Film</li><li>Nyheter</li><li>Dokumentar</li></ul>');

} 

function Show(name, programid, url) {
    this.type = 'TVProgram';
    this.name = name;
    this.programid = programid;
    this.url = url;
}

Show.prototype.permalink = function() {
    if(this.url !== null)
        return this.url;
    return 'http://tv.nrk.no/program/'+this.programid;
};

function Series(seriesid, title, thumb) {
    this.seriesid = seriesid;
    this.title = title;
    this.thumb = thumb;
    this.seasons = [];
    var self = this;
    this.metadataurl = function() {
        return 'http://dataflyt.nrk.no/dataflow/program/metadata/v0.1/series/'+self.seriesid;
    };
}

Series.prototype.pullmetadata = function() {
    console.log("pullmetadata:%o", this.metadataurl);
};

function Season(seasonid, seriesobj) {
    this.seasonid = seasonid;
    this.series = seriesobj;
    this.episodes = [];
    var self = this;
    this.metadataurl = function() {
        if(self.series.seriesid === undefined || self.seasonid === undefined)
            throw 'NotInitialized'
        return 'http://dataflyt.nrk.no/dataflow/program/metadata/v0.1/series/'+self.series.seriesid+'/seasons/'+self.seasonid;
    };
    this.episodesurl = function() {
        var _u = this.metadataurl();
        return _u + '/episodes';
    };
    this.pullepisodes(); // get all episodes
}

Season.prototype.pullmetadata = function() {
    console.log("pullmetadata:%o", this.metadataurl());
    jQuery.get(this.metadataurl(), function(response) {
        //pass
    }, 'xml');
};

Season.prototype.pullepisodes = function() {
    console.log("pullepisodes:%o", this.episodesurl());
    var self = this;
    
    jQuery.get(this.episodesurl(), function(xml) {
        self.episodes = [];
        jQuery(xml).find("objects").children().each(function(i) {
            //console.log( i + " episode: " + $( this ).attr("dataid") );
            var o = $(this);
            var e = new Episode(o.attr("dataid"), self.series, self, i+1);
            self.episodes.push(e);
        });
        $("body").trigger("episodesupdated");
 
    }, 'xml');
};

Season.prototype.deltaepisode = function(from, delta) {
    for (var i = 0; i < this.episodes.length - 1; i++) {
        console.debug("deltaepisode %o ", this.episodes[i].programid);
        if (this.episodes[i].programid.toUpperCase() == from.programid.toUpperCase()) {
            try {
                return this.episodes[i + delta];
            } catch(Exception) {
                return undefined;
            }
        }
    };
};

function Episode(programid, series, season, episodeno, url) {
    Show.call(this);
    this.type = 'TVEpisode';
    this.programid = programid;
    this.series = series || new Series();
    this.season = season || new Season();
    this.episode = episodeno || -1;
    this.url = url;
    var self = this;
    this.metadataurl = function() {
        return 'http://dataflyt.nrk.no/dataflow/program/metadata/v0.1/series/'+self.series.seriesid+'/seasons/'+self.season.seasonid+'/episodes';
    };
    this.viewurl = function() {
        return 'http://tv.nrk.no/serie/'+self.series.seriesid+'/'+self.programid;
    }
}

Episode.prototype.pullmetadata = function() {
    console.log("pullmetadata:%o", this.metadataurl);
};

Episode.prototype.next = function() {
    return this.season.deltaepisode(this, 1);
};

Episode.prototype.prev = function() {
    return this.season.deltaepisode(this, -1);
};

Episode.prototype.parse_episodes = function(s) {
    // s = "4:6"
    var spl = s.split(":");
    this.episode = parseInt(spl[0], 10);
    this.episodes = parseInt(spl[1], 10);
};

Episode.prototype.permalink = function() {
    if(this.url !== null)
        return this.url;
    return 'http://tv.nrk.no/serie/'+this.name+'/serie-'+this.series+'/episode-'+this.episode;
};

function parse_meta() {
    // read <meta/> tags and determine if this page holds an independent Program, or a Series Episode, 
    // and get relevant meta data
    var programid = $('meta[name="programid"]').attr('content');
    var title = $('meta[name="title"]').attr('content');
    var path = $('link[rel="canonical"]').attr('href').replace('http://tv.nrk.no', '');
    var type = $('meta[name="type"]').attr('content');
    if(type == 'program') {
        // independent Program, e.g. documentary or movie
        return new Show(title, programid, path)
    } else if(type == 'episode') {
        // an episode of a series
        var seriesid = $('meta[name="seriesid"]').attr('content');
        var seriestitle = $('meta[name="seriestitle"]').attr('content');
        var series = new Series(seriesid, seriestitle);

        var seasonid = $('meta[name="seasonid"]').attr('content');
        var season = new Season(seasonid, series);

        var episodenumber = $('meta[name="episodenumber"]').attr('content');
        return new Episode(programid, series, season, episodenumber, path);
    }
}

function favorite(el) {
    console.log("fave %o", el);
}

function favorite_series(el) {
    console.log("fave_series %o", el);
}

function watch_later(ev) {
    console.debug('wach_later: %o', show.programid);
    store.watch_later(show.programid);
    fbxhr.post_wants_to_watch(show.permalink());
}



///////// VIEW CODE

//var sofaplayer = $("#sofavideo");


$("body").on("episodesupdated", function() {
    var next = show.next();
    var prev = show.prev();
    console.log('next: %o, prev: %o', next, prev)
    if(prev)
        jQuery('<a id=sofa-prev class="lp-link nav-link" href=#>Forrige</a>').click(function(ev) { goto(prev.viewurl()); }).prependTo('nav.global-nav');
    if(next)
        jQuery('<a id=sofa-next class="lp-link nav-link" href=#>Neste</a>').click(function(ev) { goto(next.viewurl()); }).prependTo('nav.global-nav');
});

$('body').on('watchlistupdated', function(ev, watchlist) {
    console.debug('new watchlist: %o', watchlist);
    $('#sofa-watchlist').text('▤ ('+watchlist.length+')');
});

$('body').on('current_show', function() {
    console.log('current_show: %o', show);
});

function updateStatusCallback(status) {
    console.log("fB statsu. %o", status);
    fbstatus = status;
    fbxhr.access_token = status.authResponse.accessToken;
    store.set_user_access_token(status.authResponse.accessToken);
    store.set_user_id(status.authResponse.userID);
    if(status.status == 'connected') {
        // pull things from face
        var is_nrk = function(element) {
            return element.href.indexOf('http://tv.nrk.no') !== -1;
        }
        fbxhr.get_wants_to_watch(function(data) {
            console.log('get_watch_ %o', data);
            data
                .filter(is_nrk)
                .forEach(function(element) {
                    jQuery('<li>'+element+'</li>').appendTo('#queue ul');
                });
        });
    }
}

var bg = {
    send : function(msg, cb) {
        var _cb = cb || function(response) { console.log('got response: %o', response); };
        chrome.runtime.sendMessage(msg, _cb);
    },
    FBAuth : function(app_id) {
        var msg = {fb_cmd: 'auth', fb_app_id: app_id};
        bg.send(msg);
    }
};

$(document).ready(function() {
    var appId= '1403365179880383';
    $('body').trigger('watchlistupdated', [store.get_watch_later()]);
    var lastPlayheadPosition, player;
    var playerWrapper = jQuery('#playerelement');
    console.debug('page contains player:', playerWrapper.length > 0);
    if(playerWrapper.length > 0) {
        // we have a player on the page (but the flash player may not have been initialised yet)
        $('#sfMain').jcarousel({
            list: '#main',
            items: '#main .box'  
        });
        jQuery('<a id=sofa-playpause class="lp-link nav-link" href=#>▶</a>').click(playpause).prependTo('nav.global-nav');
        jQuery('<a id=sofa-later class="lp-link nav-link" href=#>▤</a>').click(watch_later).prependTo('nav.global-nav');
        show = parse_meta();
        console.debug("show: %o", show);
        $('body').trigger('current_show', [show]); // broadcast current show
        //sofaplayer.insertAfter("#playerelement")
        //sofaplayer.attr("src", $("#playerelement").data("media"));
        /*
        chrome.runtime.sendMessage({cmd:'playvideo',
                                    url:$("#playerelement").data("media")
                                   });
        */
        lastPlayheadPosition = -1;
        player = new FlashPlayer();
        window.setInterval(function() {
            if(player.obj === null) { // check to see if the flash player really exists
                player = new FlashPlayer();
                if(player.obj === null) return;
            }
            var playheadPosition = player.position();
            if(playheadPosition > lastPlayheadPosition) {
                playerWrapper.trigger('is_playing', [playheadPosition]);
                var time_left = player.duration() - playheadPosition;
                if(playheadPosition > 1 && time_left < 30) {
                    console.debug('duration:%o, playhead:%o', player.duration(), playheadPosition);
                    // less than 10 seconds left of movie
                    playerWrapper.trigger('is_finishing', [time_left]);
                }
            } else {
                playerWrapper.trigger('is_stopped', [playheadPosition]);
            }
            lastPlayheadPosition = playheadPosition;

        }, 1000); // run every second
        playerWrapper.on('is_playing', function(ev, pos) {
            console.debug('player is playing: %o', pos);
            jQuery('#sofa-playpause').text('❚❚');
            var t = window.document.title;
            if(t.indexOf('▶') == -1)
                window.document.title = '▶ '+t;
        });
        playerWrapper.on('is_stopped', function(ev, pos) {
            jQuery('#sofa-playpause').text('▶');
            window.document.title = window.document.title.replace('▶ ', '');
        });
        playerWrapper.on('is_finishing', function(ev, remaining_seconds) {
            jQuery('#sofa-playpause').text('-'+parseInt(remaining_seconds, 10));
            if(remaining_seconds < 15) {
                console.log("want to go to next episode:%o", show.next().viewurl());
                //goto(show.next().viewurl());
                window.document.title = window.document.title.replace('▶ ', '');
            }
        });
        playerWrapper.on('playpause', function(ev) {
            player.playpause();
        });

    } // end if(player on page)

    if(jQuery('h2[itemtype="http://schema.org/TVSeries"]').prepend('Serie: ').length) {
        // this is a tv series
        //show.season.setseasonid($('meta[name="seasonid"]').attr('content'));
        jQuery('<a id=sofa-favorite-series class="lp-link nav-link" href=#>Serie: ⭒</a>').click(favorite_series).prependTo('nav.global-nav');
        //jQuery('<a id=sofa-favorite-episode class="lp-link nav-link" href=#>Ep: ⭒</a>').click(favorite).prependTo('nav.global-nav');
        //console.log(show);

    }

    jQuery('<a class=sofa-later href=#>▤</a>')
        .appendTo('.listobject-title')
        .click(watch_later);
    //jQuery('nav.global-nav').prepend('<a id=sofa-favs class="lp-link nav-link" href=#>Nytt</a>').click(openNew);

    //bg.FBAuth(appId);
    /*
    FB.init({
        "appId": '1403365179880383',
      //channelUrl: '//yourapp.com/channel.html',
        "frictionlessRequests": true,
        "init": true,
        "level": "debug",
        "signedRequest": null,
        "status": true,
        "trace": false,
        "version": "mu",
        "viewMode": "website",
        "xfbml": false,
        "cookie": false,
        "autoRun": true

    });     
    FB.getLoginStatus(updateStatusCallback);
    FB.Event.subscribe('auth.authResponseChange', updateStatusCallback);
    */
});
