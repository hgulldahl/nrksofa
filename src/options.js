// use strict;

var fbstatus;

function login() {
    FB.login();
}

function logout() {
    FB.logout();
}

function updateStatusCallback(status) {
    console.log("fB statsu. %o", status);
    fbstatus = status;
    if(status.status == 'connected') {
        $('#login').attr('disabled','disabled');
        $('#logout').attr('disabled','');
    } else {
        $('#logout').attr('disabled','disabled');
        $('#login').attr('disabled','');
    }
    $('#console').text(status);
    FB.api('/me', function(response) {
         console.log('Good to see you, %o', response);
    });
}

$(document).ready(function() {
    $('#logout').click(logout);
    $('#login').click(login);
    FB.init({
        "appId": '1403365179880383',
      //channelUrl: '//yourapp.com/channel.html',
        "frictionlessRequests": true,
        "init": true,
        "level": "debug",
        "signedRequest": null,
        "status": true,
        "trace": false,
        "version": "mu",
        "viewMode": "website",
        "xfbml": false,
        "cookie": false,
        "autoRun": true

    });     
    FB.getLoginStatus(updateStatusCallback);
    FB.Event.subscribe('auth.authResponseChange', updateStatusCallback);
});
